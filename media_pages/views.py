from django.shortcuts import render, get_object_or_404
from .models import Image, ImageLineItems, Video
from django.core.paginator import Paginator


def landing_page(request):
    template = 'media_pages/media_landing_page.html'

    return render(request, template)


def videos(request):
    videos = Video.objects.all().order_by("-id")
    paginator = Paginator(videos, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj
    }
    template = 'media_pages/videos.html'

    return render(request, template, context)


def media_pages_portraiture(request):
    # images = Image.objects.all().filter(category="portraiture").order_by("-id")
    single_images = ImageLineItems.objects.all().filter(category="portraiture").order_by("-id")
    paginator = Paginator(single_images, 9)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj,
    }
    template = 'media_pages/media.html'

    return render(request, template, context)


def media_pages_landscape(request):
    single_images = ImageLineItems.objects.all().filter(category="landscape").order_by("-id")
    paginator = Paginator(single_images, 9)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj
    }
    template = 'media_pages/media.html'

    return render(request, template, context)


def media_pages_product(request):
    single_images = ImageLineItems.objects.all().filter(category="product").order_by("-id")
    paginator = Paginator(single_images, 9)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj
    }
    template = 'media_pages/media.html'

    return render(request, template, context)


def view_single_image(request, id):
    image = get_object_or_404(ImageLineItems, pk=id)
    context = {
        'image': image,
    }
    template = 'media_pages/single_image.html'

    return render(request, template, context)
