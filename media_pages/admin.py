from django.contrib import admin
from .models import Image, ImageLineItems, Video

class ImageLineItemsAdminInLine(admin.TabularInline):

    model = ImageLineItems

class ImageAdmin(admin.ModelAdmin):

    inlines = (ImageLineItemsAdminInLine,)
    
    fields = ['name',]

    list_display = ['name',]


admin.site.register(Image, ImageAdmin)


class VideoAdmin(admin.ModelAdmin):

    fields = ('name', 'url', 'description')

    list_display = ('name', 'description')

admin.site.register(Video)