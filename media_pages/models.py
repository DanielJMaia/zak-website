from django.db import models


class Image(models.Model):
    name = models.CharField(max_length=254)
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = 'Image Category'


class ImageLineItems(models.Model):
    class Meta:
        verbose_name_plural = 'Individual Images'

    PORTRAITURE = 'portraiture'
    LANDSCAPE = 'landscape'
    PRODUCT = 'product'
    CATEGORY = [
        (PORTRAITURE, 'Portraiture'),
        (LANDSCAPE, 'Landscape'),
        (PRODUCT, 'Product'),
    ]
    key = models.ForeignKey(Image,
                            on_delete=models.CASCADE,
                            related_name="image")
    name = models.CharField(max_length=254)
    image = models.ImageField(null=True, blank=True)
    thumbnail = models.ImageField(null=True, blank=True)
    category = models.CharField(
        max_length=32,
        choices=CATEGORY,
        default=PORTRAITURE,
    )
    location = models.CharField(max_length=254, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    upload_date = models.DateField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.name

class Video(models.Model):
    name = models.CharField(max_length=254)
    url = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name