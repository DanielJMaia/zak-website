from django.apps import AppConfig


class MediaPagesConfig(AppConfig):
    name = 'media_pages'
