from django.urls import path
from . import views

urlpatterns = [
    path('landing_page/', views.landing_page,
         name='landing_page'),
    path('video/', views.videos,
         name='videos'),
    path('portraiture/', views.media_pages_portraiture,
         name='media_pages_portraiture'),
    path('landscape/', views.media_pages_landscape, name='media_pages_landscape'),
    path('product/', views.media_pages_product,
         name='media_pages_product'),
    path('<int:id>', views.view_single_image, name='view_single_image'),
]
