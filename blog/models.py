from django.db import models


class Blog(models.Model):
    thumbnail = models.ImageField(null=True, blank=True)
    title = models.CharField(max_length=254)

    blog_image_1 = models.ImageField(null=True, blank=True)
    sub_header_1 = models.CharField(max_length=254, null=True, blank=True)
    body_text_1 = models.TextField(null=True, blank=True)

    blog_image_2 = models.ImageField(null=True, blank=True)
    sub_header_2 = models.CharField(max_length=254, null=True, blank=True)
    body_text_2 = models.TextField(null=True, blank=True)

    blog_image_3 = models.ImageField(null=True, blank=True)
    sub_header_3 = models.CharField(max_length=254, null=True, blank=True)
    body_text_3 = models.TextField(null=True, blank=True)

    blog_image_4 = models.ImageField(null=True, blank=True)
    sub_header_4 = models.CharField(max_length=254, null=True, blank=True)
    body_text_4 = models.TextField(null=True, blank=True)

    blog_image_5 = models.ImageField(null=True, blank=True)
    sub_header_5 = models.CharField(max_length=254, null=True, blank=True)
    body_text_5 = models.TextField(null=True, blank=True)

    blog_image_6 = models.ImageField(null=True, blank=True)
    sub_header_6 = models.CharField(max_length=254, null=True, blank=True)
    body_text_6 = models.TextField(null=True, blank=True)

    author = models.CharField(max_length=254)
    date = models.DateField(null=False, blank=False)

    def __str__(self):
        return self.title +  ' - ' + self.author