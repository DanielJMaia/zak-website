from django.contrib import admin
from .models import Blog

class BlogAdmin(admin.ModelAdmin):

    fields = ('thumbnail', 'title',
              'blog_image_1', 'sub_header_1', 'body_text_1',
              'blog_image_2', 'sub_header_2', 'body_text_2',
              'blog_image_3', 'sub_header_3', 'body_text_3',
              'blog_image_4', 'sub_header_4', 'body_text_4',
              'blog_image_5', 'sub_header_5', 'body_text_5',
              'blog_image_6', 'sub_header_6', 'body_text_6',
              'author', 'date')

    list_display = ('title', 'author', 'date',)

admin.site.register(Blog)
