from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views


urlpatterns = [
    path('all/', views.all_posts, name='all_posts'),
    path('<int:id>/', views.view_single_post, name='view_single_post'),
    path('add/', views.add_post, name='add_post'),
    path('edit/<int:id>/', views.edit_post, name='edit_post'),
    path('delete/<int:id>/', views.delete_post, name='delete_post'),
]
