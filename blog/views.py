from django.core.paginator import Paginator
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Blog
from .forms import BlogForm


def all_posts(request):
    """A view to see all blogposts"""
    blogpost = Blog.objects.all().order_by("date")
    paginator = Paginator(blogpost, 10)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    template = 'blog/blogposts.html'
    context = {
        'page_obj': page_obj
    }

    return render(request, template, context)


def view_single_post(request, id):
    """A view to read a single post"""
    post = get_object_or_404(Blog, pk=id)
    template = 'blog/single_blog_post.html'
    context = {
        'post': post,
    }

    return render(request, template, context)


@login_required
def add_post(request):
    """add a new blogpost"""
    if not request.user.is_superuser:
        messages.error(request, "Sorry, only the site owner can do that.")
        return redirect(reverse('home'))

    if request.method == 'POST':
        form = BlogForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save()
            messages.success(request, "Successfully added a blogpost!")
            return redirect(reverse('view_single_post', args=[post.id]))
        else:
            messages.error(request, 'Failed to add blogpost. Please ensure the form is valid')
    else:
        form = BlogForm()
        template = 'blog/add_post.html'
        context = {
            'form': form,
        }

    return render(request, template, context)


@login_required
def edit_post(request, id):
    """Edit a blog post"""
    if not request.user.is_superuser:
        messages.error(request, "Sorry, only the site owner can do that.")
        return redirect(reverse('home'))

    post = get_object_or_404(Blog, pk=id)
    if request.method == 'POST':
        form = BlogForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            form.save()
            messages.success(request, f'Successfully updated blogpost { post.title }')
            return redirect(reverse('view_single_post', args=[post.id]))
        else:
            messages.error(request, 'Failed to update blogpost. Please ensure the form is valid.')
    else:
        form = BlogForm(instance=post)
        messages.info(request, f'You are editing {post.title}')

    template = 'blog/edit_post.html'
    context = {
        'form': form,
        'post': post,
    }

    return render(request, template, context)


@login_required
def delete_post(request, id):
    """ Delete a blogpost in the store """
    if not request.user.is_superuser:
        messages.error(request, "Sorry, only store owners can do that.")
        return redirect(reverse('home'))

    post = get_object_or_404(Blog, pk=id)
    post.delete()
    messages.success(request, 'Post deleted!')
    return redirect(reverse('all_posts'))
