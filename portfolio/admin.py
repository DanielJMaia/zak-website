from django.contrib import admin
from .models import About, SubjectLineItems


class SubljectLineItemsAdminInLine(admin.TabularInline):

    model = SubjectLineItems


class AboutAdmin(admin.ModelAdmin):
    inlines = (SubljectLineItemsAdminInLine,)

    fields = ('title',
              'textbox_1',
              'textbox_2',
              'textbox_3',
              'experience_text',
              'image',
              'passion_text')
    list_display = ('title',)


admin.site.register(About, AboutAdmin)
