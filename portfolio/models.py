from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class About(models.Model):
    title = models.CharField(max_length=254)
    textbox_1 = models.TextField(null=True, blank=True)
    textbox_2 = models.TextField(null=True, blank=True)
    textbox_3 = models.TextField(null=True, blank=True)
    experience_text = models.TextField(null=True, blank=True)
    image = models.ImageField(null=True, blank=True)
    passion_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class SubjectLineItems(models.Model):
    name = models.CharField(max_length=254, default=None)
    key = models.ForeignKey(About,
                            on_delete=models.CASCADE,
                            related_name='about')
    experience = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(100)])
    subject_image = models.ImageField(null=False, blank=False, default='default.png')

    def __str__(self):
        return self.name
