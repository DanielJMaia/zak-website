from django.shortcuts import render
from .models import About, SubjectLineItems


def portfolio(request):
    portfolios = About.objects.all()
    subjects = SubjectLineItems.objects.all()
    context = {
        'portfolios': portfolios,
        'subjects': subjects,
    }
    return render(request, 'portfolio/portfolio.html', context)
